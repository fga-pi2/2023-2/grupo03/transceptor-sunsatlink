#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#define SPI_SLCK_PIN 18
#define SPI_MISO_PIN 19
#define SPI_MOSI_PIN 23
#define SPI_NSS_PIN 5

#define LORA_NRESET_PIN 4
#define LORA_DIO0_PIN 2     // Optional, not used
#define LORA_FRQUENCY 433E6 // 433 MHz, frequency of the module LoRa
#define LORA_SYNC_WORD 75   // Value between 0-255, used to sync the receiver and transmitter

#endif // _CONSTANTS_H
