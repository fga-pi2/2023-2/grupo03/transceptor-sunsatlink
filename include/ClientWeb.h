#ifndef CONFIGSERVER_H
#define CONFIGSERVER_H

#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <HTTPClient.h>
#include <WiFi.h>

typedef struct ApiPacketIncoming
{
  int time;
  float azimuth;
  float elevation;
} ApiPacketIncoming;

class ClientWeb
{
public:
  ClientWeb();
  void sendGetRequest();
  void sendPostRequest(String json);
  ApiPacketIncoming getApiPacketIncoming() const;
  
private:
  ApiPacketIncoming apiPacketIncoming;

};

#endif // CONFIGSERVER_H