#ifndef _LORA_COMM_H
#define _LORA_COMM_H

#include "Arduino.h"

typedef struct LoRaDataPacketIncoming
{
  unsigned char length;
  float longitude;
  float latitude;
  float altitude;
  float azimuth;
  float elevation;
} LoRaDataPacketIncoming;

typedef struct LoRaDataPacket
{
  unsigned char length;
  int time;
  float azimuth;
  float elevation;
} LoRaDataPacket;

class LoRaComm
{
public:
  LoRaComm();
  void initialize();
  unsigned long getLastSendTime();
  unsigned long getLastReceiveTime();
  String getPacketJson();
  void setPacket(LoRaDataPacket packet);
  void sendMessage();
  void recvMessage(void (*callback)());
  void reset();

private:
  unsigned long lastSendTime;
  unsigned long lastReceiveTime;
  LoRaDataPacket packet;
  LoRaDataPacketIncoming incomingPacket;
};

#endif // _LORA_COMM_H
