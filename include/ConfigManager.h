#ifndef configManager_h
#define configManager_h

#include <Arduino.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>

class ConfigManager
{
public:
  void begin();
  void setFileName(const char *fileName);
  void writeConfigContent(const String &content);
  String getSsid() const;
  String getPassword() const;

private:
  const char *_fileName = "/config.json";
  String _configContent = "";
  String ssid = "";
  String password = "";

  void initialize();
  void readConfig();
  String getConfigContent() const;
};

#endif
