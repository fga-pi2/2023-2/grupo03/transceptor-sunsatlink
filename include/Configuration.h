#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

// Access point configuration
#define WIFI_SSID       "SunsatLink"
#define WIFI_PASSWORD   "sunsatlink"

// Web server configuration
#define WEB_SERVER_PORT     80
#define WEB_SERVER_TIMEOUT  15000

// API Server configuration
#define SERVER_URL              "https://sunsat-655c9e75e840.herokuapp.com:"
#define SERVER_PORT             443
#define SERVER_GET_ENDPOINT     "/satellite/monitoring/"
#define SERVER_POST_ENDPOINT    "/antenna/position/"
#define SERVER_REQUEST_INTERVAL 30000

// LoRa configuration
#define LORA_SEND_INTERVAL  10000
#define LORA_RECV_TIMEOUT   60000

#endif // _CONFIGURATION_H