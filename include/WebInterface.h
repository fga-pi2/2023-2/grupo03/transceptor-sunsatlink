#ifndef WEB_INTERFACE_H
#define WEB_INTERFACE_H

#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <SPIFFS.h>

#include "ConfigManager.h"
#include "Configuration.h"

class WebInterface
{
public:
  WebInterface(ConfigManager &config_ref);
  void begin();
  void messageCheck();
  void close();
  void checkWiFiConection();

private:
  AsyncWebServer server{WEB_SERVER_PORT};
  AsyncWebSocket ws{"/ws"};
  bool newMessageAvailable = false;
  AsyncWebSocketClient *connectedClient = NULL;
  String receivedMessage;
  ConfigManager *configManager = nullptr;

  void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len);
};

#endif
