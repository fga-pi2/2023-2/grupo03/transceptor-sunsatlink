# SunsatLink

O SunsatLink representa a iniciativa do Grupo 3 na disciplina de Projeto Integrador de Engenharia 2. Este grupo é composto por estudantes das áreas de engenharia aeroespacial, automotiva, eletrônica, energia e software, pertencentes ao Campus Faculdade do Gama da Universidade de Brasília. O propósito central deste projeto consiste em conceber uma solução de movimentação de uma antena de maneira que a mesma consiga captar corretamente os dados de um satélite pelo período em que o mesmo estiver visível a ela.

# Sistema de Comunicação SunsatLink

O Sistema de Comunicação SunsatLink tem como objetivo retransmitir requisições do [Backend SunsatLink](https://gitlab.com/fga-pi2/2023-2/grupo03/backend-sunsatlink) via rádio LoRa (*Long Range*) para o [Sistema de Acionamento dos Motores SunsatLink](https://gitlab.com/fga-pi2/2023-2/grupo03/sistema-de-controle-sunsatlink) e vide-versa, possibilitando controle e monitoramento da antena em áreas remotas.

# Configuração do Ambiente de Desenvolvimento (PlatformIO)

O código foi elaborado utilizando o PlatformIO, uma plataforma dedicada ao desenvolvimento de soluções para sistemas embarcados.

## Pré-requisito(s) de Hardware

Antes de começar, certifique-se de ter o seguinte(s) componente(s)/dispositivo(s):

- **Esp32**

- **Módulo LoRa SX1278**

## Pré-requisito(s) de Software

Antes de começar, certifique-se de ter o seguinte(s) pré-requisito(s) instalado(s) em seu sistema:

- **IDE PlatformIO:** Você pode usar o PlatformIO com diferentes IDEs, como Visual Studio Code, Atom, ou Eclipse. Instale o IDE de sua escolha conforme as instruções em [https://platformio.org/platformio-ide](https://platformio.org/platformio-ide).

# Compilação do Código (VSCode)

Com a Esp32 conectada ao computador, e o projeto aberto, siga os passos a seguir:

**Passo 1:** Selecione a aba do PLatformIO.

![](./assets/ManualDeUso_Passo1.png)

**Passo 2:** Gere a compilação do sistema de arquivos.

![](./assets/ManualDeUso_Passo2.png)

**Passo 3:** Envie o sistema de arquivos para a Esp32.

![](./assets/ManualDeUso_Passo3.png)

**Passo 4:** Gere a compilação do código-fonte e envie para a Esp32.

![](./assets/ManualDeUso_Passo4.png)

# Manual Uso

Com a Esp32 ligada, por ```1 minuto e 30 segundos``` um rede **Acess Point**, com ```SSID SunsatLink``` estará disponível, conecte com utilizando a senha ```sunatlink``` e acesse com seu navegador o endereço ```192.168.4.1```, será então exibida a seguinte página de cadastro da rede que deseja com a qual a Esp32 se conectará.

![](./assets/Transceptor_MenuDeConfiguracao.png)

Após o preenchimento do formulário, basta clicar em salvar e dispositivo reiniciará, sempre quando ligar estará disponível o **Acess Point** novamente.

# Variáveis de Ambiente

Todas configuração do projeto podem ser encontradas em [Configuration.h](./include/Configuration.h):

| Variável | Descrição | Valor Padrão |
| --- | --- | --- |
| `WIFI_SSID` | Nome da rede WiFi do **Acess Point** | `"SunsatLink"` |
| `WIFI_PASSWORD` | Senha da rede WiFi do **Acess Point** | `"sunsatlink"` |
| `WEB_SERVER_TIMEOUT` | Tempo de espera para o servidor web de configuração da rede | `90000` (90 segundos) |
| `WEB_SERVER_PORT` | Porta do servidor web | `80` (http) |
| `SERVER_URL` | URL do servidor | `"https://sunsat-655c9e75e840.herokuapp.com:"` |
| `SERVER_PORT` | Porta do servidor | `443` (https) |
| `SERVER_GET_ENDPOINT` | Endpoint do servidor para recebimento de movimentação da antena | `"/satellite/monitoring/"` |
| `SERVER_POST_ENDPOINT` | Endpoint do servidor para envio dos dados de monitoramento dos sensores | `"/antenna/position/"` |
| `SERVER_REQUEST_INTERVAL` | Intervalo de tempo entre as requisições ao servidor | `30000` (30 segundos) |
| `LORA_SEND_INTERVAL` | Intervalo de tempo entre os envios de dados via LoRa () | `10000` (10 segundos) |
| `LORA_RECV_TIMEOUT` | Tempo de espera máximo para recebimento de um pacote, após o tempo módulo LoRa é reiniciado | `60000` (60 segundos) |

Para configurações de hardware ou visualização da pinagem para o(s) componente(s), consulte [Constants.h](./include/Constants.h). Segue abaixo o esquemático:

![](./assets/Esquematico.png)

# Pontos a Serem Melhorados

Atualmente o sistema de comunicação está funcionando de forma satisfatória, porém, existem alguns pontos que podem ser melhorados:

- Possibilidade de configuração no qual permita conectar em uma rede WiFi que necessite de usuário e senha. No momento apenas redes que necessitam de senha são suportadas.
- Possibilidade de configuração da rede WiFi via Bluetooth, para que não seja necessário o uso do **Acess Point**.
- Abrir **Acess Point** apenas na primeira vez que a Esp32 é ligada, ou quando pressionar um botão por alguns segundos (restauração de fábrica). No momento o **Acess Point** é aberto sempre que a Esp32 é ligada, por 1 minuto e 30 segundos.
