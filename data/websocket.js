var ws = new WebSocket("ws://192.168.4.1/ws"); // Substitua pelo IP do seu ESP32
var logged = false;
var modelLoaded = false;

ws.onopen = function(event) {
  console.log("WebSocket connected.");
};

ws.onclose = function(event) {
  console.log("WebSocket disconnected.");
};

function sendMessage(message) {
  ws.send(message);
}


function saveConfig() {
  var ssid = document.getElementById('ssidWifi').value;
  var password = document.getElementById('passwordWifi').value;

  var configData = {
    ssid: ssid,
    password: password
  };

  // Converte o objeto em JSON
  var configJson = JSON.stringify(configData);

  sendMessage(configJson);

}

// Adicionar o manipulador de evento ao botão "Abrir Pop-up"
document.getElementById('saveButton').addEventListener('click', function() {
  saveConfig();
});
