#include "ClientWeb.h"
#include "Configuration.h"

ClientWeb::ClientWeb()
{
}

ApiPacketIncoming ClientWeb::getApiPacketIncoming() const {
    return apiPacketIncoming;
}

void ClientWeb::sendGetRequest()
{
    HTTPClient httpClient;
    httpClient.begin(SERVER_URL + String(SERVER_PORT) + SERVER_GET_ENDPOINT);

    Serial.println("GET request");

    int httpCode = httpClient.GET();

    if (httpCode > 0)
    {
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK)
        {
            String payload = httpClient.getString();
            DynamicJsonDocument doc(128);
            deserializeJson(doc, payload);

            apiPacketIncoming.time = doc["time"];
            apiPacketIncoming.azimuth = doc["azimuth"];
            apiPacketIncoming.elevation = doc["elevation"];

            Serial.println("API Packet Incoming:");
            Serial.println("- " + String(apiPacketIncoming.time));
            Serial.println("- " + String(apiPacketIncoming.azimuth));
            Serial.println("- " + String(apiPacketIncoming.elevation));
            Serial.println();
        }
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", httpClient.errorToString(httpCode).c_str());
    }
    httpClient.end();
}

void ClientWeb::sendPostRequest(String json)
{

    HTTPClient httpClient;
    httpClient.begin(SERVER_URL + String(SERVER_PORT) + SERVER_POST_ENDPOINT);

    Serial.println("POST request");

    int httpCode = httpClient.POST(json);

    if (httpCode > 0)
    {
        Serial.printf("[HTTP] POST... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK)
        {
            String payload = httpClient.getString();
            Serial.println(payload);
        }
    }
    else
    {
        Serial.printf("[HTTP] POST... failed, error: %s\n", httpClient.errorToString(httpCode).c_str());
    }

    httpClient.end();
}
