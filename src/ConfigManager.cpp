#include "ConfigManager.h"

void ConfigManager::begin()
{
  if (!SPIFFS.begin(true))
  {
    Serial.println("An error occurred while mounting SPIFFS");
  }

  readConfig();
  initialize();
}

void ConfigManager::initialize()
{
  DynamicJsonDocument jsonDoc(1024);
  DeserializationError error = deserializeJson(jsonDoc, _configContent);

  if (error)
  {
    Serial.println("Failed to parse JSON");
    Serial.println(error.c_str());
    return;
  }

  if (jsonDoc.containsKey("ssid"))
  {
    ssid = jsonDoc["ssid"].as<String>();
  }
  if (jsonDoc.containsKey("password"))
  {
    password = jsonDoc["password"].as<String>();
  }
}

void ConfigManager::setFileName(const char *fileName)
{
  _fileName = fileName;
}

void ConfigManager::readConfig()
{
  File configFile;

  if (SPIFFS.exists(_fileName))
  {
    configFile = SPIFFS.open(_fileName, "r");
    Serial.println("Arquivo de configuração encontrado");
  }
  else
  {
    Serial.println("Arquivo de configuração não encontrado");
    return;
  }

  _configContent = configFile.readString();

  configFile.close();
}

String ConfigManager::getConfigContent() const
{
  return _configContent;
}

void ConfigManager::writeConfigContent(const String &content)
{
  File configFile = SPIFFS.open(_fileName, "w");
  if (!configFile)
  {
    Serial.println("Failed to open config file for writing");
    return;
  }

  configFile.print(content);

  configFile.close();
  initialize();
}

String ConfigManager::getSsid() const
{
  return ssid;
}

String ConfigManager::getPassword() const
{
  return password;
}
