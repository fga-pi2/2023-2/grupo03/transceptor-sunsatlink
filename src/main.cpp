#include <Arduino.h>
#include <SPI.h>
#include <LoRa.h>
#include <WiFi.h>

#include "WebInterface.h"
#include "ConfigManager.h"
#include "ClientWeb.h"
#include "LoRaComm.h"
#include "Constants.h"
#include "Configuration.h"

ConfigManager configManager;
WebInterface myWebInterface(configManager);
ClientWeb clientWeb;
LoRaComm loRaComm;

unsigned long lastPostTime = 0;

void loRaCommTask(void *pvParameters);

void setupSerial(void)
{
  Serial.begin(115200);
  while (!Serial)
    delay(10);
  Serial.println("Serial init succeeded :)");
}

void setup()
{
  setupSerial();

  configManager.begin();
  WiFi.begin(configManager.getSsid(), configManager.getPassword());
  myWebInterface.checkWiFiConection();

  loRaComm.initialize();
  xTaskCreatePinnedToCore(loRaCommTask, "loRaCommTask", 10000, NULL, 1, NULL, 1);
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    clientWeb.sendGetRequest();
    delay(SERVER_REQUEST_INTERVAL);
  }
  else
  {
    myWebInterface.checkWiFiConection();
  }
}

void loRaCommTask(void *pvParameters)
{
  while (1)
  {
    if (millis() - loRaComm.getLastSendTime() > LORA_SEND_INTERVAL)
    {
      LoRaDataPacket packet;
      ApiPacketIncoming clientPacket = clientWeb.getApiPacketIncoming();
      memcpy(&packet, &clientPacket, sizeof(packet));
      loRaComm.setPacket(packet);
      loRaComm.sendMessage();
    }

    if (millis() - loRaComm.getLastReceiveTime() > LORA_RECV_TIMEOUT)
    {
      loRaComm.reset();
    }

    loRaComm.recvMessage([]() {
      if (millis() - lastPostTime > SERVER_REQUEST_INTERVAL)
      {
        clientWeb.sendPostRequest(loRaComm.getPacketJson());
        lastPostTime = millis();
      }
    });
  }
}