#include "WebInterface.h"
#include "EEPROM.h"
#include "Configuration.h"

WebInterface::WebInterface(ConfigManager &config_ref) : configManager(&config_ref)
{
}

// Configurando o WiFi, a pagina web e o WebSocket
void WebInterface::begin()
{
  WiFi.softAP(WIFI_SSID, WIFI_PASSWORD);
  IPAddress ipAddress = WiFi.softAPIP();
  Serial.println("AP IP address: " + ipAddress.toString());

  if (!SPIFFS.begin(true))
  {
    Serial.println("Ocorreu um erro ao montar o SPIFFS");
    return;
  }

  server.on("/", HTTP_GET, [this](AsyncWebServerRequest *request)
            { request->send(SPIFFS, "/index.html", "text/html"); });

  server.serveStatic("/", SPIFFS, "/");

  ws.onEvent([this](AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
             { onWsEvent(server, client, type, arg, data, len); });

  server.addHandler(&ws);
  server.begin();
}

void WebInterface::close()
{
  server.end();
  WiFi.softAPdisconnect(true);
}

// Metodo de verificacao de qual mensagem chegou
void WebInterface::messageCheck()
{
  if (newMessageAvailable)
  {
    Serial.println("Nova mensagem disponível: " + receivedMessage);

    if (connectedClient)
    {

      if (receivedMessage.startsWith("{"))
      {
        DynamicJsonDocument jsonDoc(1024);
        DeserializationError error = deserializeJson(jsonDoc, receivedMessage);
        String configFileContent;
        if (!error)
        {
          if (jsonDoc.containsKey("ssid") && jsonDoc.containsKey("password"))
          {
            Serial.println("Arquivo de configuração completo");
            serializeJson(jsonDoc, configFileContent);
            configManager->writeConfigContent(configFileContent);
            ESP.restart();
          }
          else
          {
            Serial.println("Arquivo de configuração incompleto, mantendo configurações atuais");
            jsonDoc["ssid"] = configManager->getSsid();
            jsonDoc["password"] = configManager->getPassword();
            serializeJson(jsonDoc, configFileContent);
            configManager->writeConfigContent(configFileContent);
          }
        }
        else
        {
          Serial.println("Erro ao analisar JSON.");
        }
      }
      else
      {
        Serial.println("Mensagem inválida.");
      }
    }

    newMessageAvailable = false;
    receivedMessage = "";
  }
}
void WebInterface::checkWiFiConection()
{
  unsigned long startTime = millis();
  begin();
  Serial.println("Servidor aberto");
  while (millis() - startTime <= WEB_SERVER_TIMEOUT || WiFi.status() != WL_CONNECTED || connectedClient)
  {
    messageCheck();
  }
  Serial.println("Servidor fechado");
  close();
}

// Metodo de verificação de conexao de clientes e recebimento de mensagens
void WebInterface::onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  if (type == WS_EVT_CONNECT)
  {
    Serial.println("Cliente conectado.");
    connectedClient = client;
  }
  else if (type == WS_EVT_DISCONNECT)
  {
    Serial.println("Cliente desconectado.");
    connectedClient = NULL;
  }
  else if (type == WS_EVT_DATA)
  {
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    if (info->final && info->index == 0 && info->len == len)
    {
      receivedMessage = "";
      for (size_t i = 0; i < len; i++)
      {
        receivedMessage += (char)data[i];
      }
      newMessageAvailable = true;
    }
  }
}
