#ifndef LORACOMM_H
#define LORACOMM_H

#include <SPI.h>
#include <LoRa.h>
#include <ArduinoJson.h>

#include "LoRaComm.h"
#include "Constants.h"
#include "Configuration.h"

LoRaComm::LoRaComm()
{
  packet = {0, 0, 0.0, 0.0};
  incomingPacket = {0, 0.0, 0.0, 0.0, 0.0, 0.0};
  lastSendTime = 0;
  lastReceiveTime = 0;
}

void LoRaComm::initialize()
{
  LoRa.setPins(SPI_NSS_PIN, LORA_NRESET_PIN, LORA_DIO0_PIN);

  if (!LoRa.begin(LORA_FRQUENCY))
  {
    Serial.println("Starting LoRa failed :(");
    while (1)
      delay(10);
  }

  LoRa.setSyncWord(LORA_SYNC_WORD);
  LoRa.enableCrc();
  Serial.println("LoRa init succeeded :)");

  lastReceiveTime = millis();
  lastSendTime = millis() - (LORA_SEND_INTERVAL / 2);
}

unsigned long LoRaComm::getLastSendTime()
{
  return lastSendTime;
}

unsigned long LoRaComm::getLastReceiveTime()
{
  return lastReceiveTime;
}

String LoRaComm::getPacketJson()
{
  StaticJsonDocument<96> doc;

  doc["longitude"] = incomingPacket.longitude;
  doc["latitude"] = incomingPacket.latitude;
  doc["altitude"] = incomingPacket.altitude;
  doc["azimuth"] = incomingPacket.azimuth;
  doc["elevation"] = incomingPacket.elevation;

  String jsonString;
  serializeJson(doc, jsonString);
  return jsonString;
}

void LoRaComm::setPacket(LoRaDataPacket packet)
{
  this->packet = packet;
}

void LoRaComm::sendMessage()
{
  LoRa.beginPacket();
  LoRa.write(sizeof(packet));
  LoRa.write((uint8_t *)&packet.time, sizeof(packet.time));
  LoRa.write((uint8_t *)&packet.azimuth, sizeof(packet.azimuth));
  LoRa.write((uint8_t *)&packet.elevation, sizeof(packet.elevation));
  LoRa.endPacket();
  lastSendTime = millis();
}

void LoRaComm::recvMessage(void (*callback)())
{
  if (LoRa.parsePacket() == 0)
    return;

  unsigned char incomingLength = LoRa.read();
  if (incomingLength != sizeof(incomingPacket)) return;
  LoRa.readBytes((uint8_t *)&incomingPacket.longitude, sizeof(incomingPacket.longitude));
  LoRa.readBytes((uint8_t *)&incomingPacket.latitude, sizeof(incomingPacket.latitude));
  LoRa.readBytes((uint8_t *)&incomingPacket.altitude, sizeof(incomingPacket.altitude));
  LoRa.readBytes((uint8_t *)&incomingPacket.azimuth, sizeof(incomingPacket.azimuth));
  LoRa.readBytes((uint8_t *)&incomingPacket.elevation, sizeof(incomingPacket.elevation));

  Serial.println("Freq Err: " + String(LoRa.packetFrequencyError()));
  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println("Recv:");
  Serial.println("- Longittude: " + String(incomingPacket.longitude));
  Serial.println("- Latiude: " + String(incomingPacket.latitude));
  Serial.println("- Altitude: " + String(incomingPacket.altitude));
  Serial.println("- Azimuth: " + String(incomingPacket.azimuth));
  Serial.println("- Elevation: " + String(incomingPacket.elevation));
  Serial.println();

  callback();
  lastReceiveTime = millis();
  lastSendTime = millis() - (LORA_SEND_INTERVAL / 2);
}

void LoRaComm::reset()
{
  LoRa.end();
  Serial.println("Resetting LoRa...");
  delay(1000);
  initialize();
}

#endif // LORACOMM_H
